package ru.tsc.tambovtsev.tm.exception.field;

import ru.tsc.tambovtsev.tm.exception.AbstractException;

public final class PermissionException extends AbstractException {

    public PermissionException() {
        super("Error! Permission is incorrect...");
    }

}
