package ru.tsc.tambovtsev.tm.dto.request;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class TaskBindProjectRequest extends AbstractUserRequest {

    @Nullable
    private String id;

    @Nullable
    private String projectId;

    public TaskBindProjectRequest(@Nullable String token, @Nullable String id, @Nullable String projectId) {
        super(token);
        this.id = id;
        this.projectId = projectId;
    }

}
