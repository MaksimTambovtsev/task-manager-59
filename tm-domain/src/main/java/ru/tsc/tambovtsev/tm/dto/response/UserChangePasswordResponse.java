package ru.tsc.tambovtsev.tm.dto.response;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;

@NoArgsConstructor
public final class UserChangePasswordResponse extends AbstractUserResponse {

    public UserChangePasswordResponse(@Nullable UserDTO user) {
        super(user);
    }
}
