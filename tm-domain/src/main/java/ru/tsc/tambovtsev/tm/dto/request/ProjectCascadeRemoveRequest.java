package ru.tsc.tambovtsev.tm.dto.request;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;

@Getter
@Setter
@NoArgsConstructor
public class ProjectCascadeRemoveRequest extends AbstractUserRequest {

    @Nullable
    private String projectId;

    public ProjectCascadeRemoveRequest(@Nullable String token, @Nullable String projectId) {
        super(token);
        this.projectId = projectId;
    }
}
