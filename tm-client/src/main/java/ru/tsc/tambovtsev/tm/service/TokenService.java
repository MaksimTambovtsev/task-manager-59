package ru.tsc.tambovtsev.tm.service;

import lombok.Getter;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.tsc.tambovtsev.tm.api.service.ITokenService;

@Getter
@Setter
@Service
public class TokenService implements ITokenService {

    @NotNull
    private String token;

}
