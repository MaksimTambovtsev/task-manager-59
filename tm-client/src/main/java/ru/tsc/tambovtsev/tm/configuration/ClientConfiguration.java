package ru.tsc.tambovtsev.tm.configuration;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import ru.tsc.tambovtsev.tm.api.endpoint.*;
import ru.tsc.tambovtsev.tm.api.service.IPropertyService;

@ComponentScan("ru.tsc.tambovtsev.tm")
public class ClientConfiguration {

    @NotNull
    private final IPropertyService propertyService;

    public ClientConfiguration(@NotNull final IPropertyService propertyService) {
        this.propertyService = propertyService;
    }

    @Bean
    @NotNull
    public ITaskEndpoint taskEndpoint() {
        return ITaskEndpoint.newInstance(propertyService);
    }

    @Bean
    @NotNull
    public IProjectEndpoint projectEndpoint() {
        return IProjectEndpoint.newInstance(propertyService);
    }

    @Bean
    @NotNull
    public ISystemEndpoint systemEndpoint() {
        return ISystemEndpoint.newInstance(propertyService);
    }

    @Bean
    @NotNull
    public IAuthEndpoint authEndpoint() {
        return IAuthEndpoint.newInstance(propertyService);
    }

    @Bean
    @NotNull
    public IUserEndpoint userEndpoint() {
        return IUserEndpoint.newInstance(propertyService);
    }

    @Bean
    @NotNull
    public IDomainEndpoint domainEndpoint() {
        return IDomainEndpoint.newInstance(propertyService);
    }

}
