package ru.tsc.tambovtsev.tm.configuration;

import lombok.NonNull;
import org.hibernate.cfg.Environment;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.jpa.JpaTransactionManager;
import org.springframework.orm.jpa.LocalContainerEntityManagerFactoryBean;
import org.springframework.orm.jpa.vendor.HibernateJpaVendorAdapter;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import ru.tsc.tambovtsev.tm.api.service.property.IDatabasePropertyService;
import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScan("ru.tsc.tambovtsev.tm")
public class ServerConfiguration {

    @Autowired
    private IDatabasePropertyService databaseProperties;

    @Bean
    public DataSource dataSource() {
        @NotNull final DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(databaseProperties.getDatabaseDriver());
        dataSource.setUrl(databaseProperties.getDatabaseUrl());
        dataSource.setUsername(databaseProperties.getDatabaseUsername());
        dataSource.setPassword(databaseProperties.getDatabasePassword());
        return dataSource;
    }

    @Bean
    public LocalContainerEntityManagerFactoryBean entityManagerFactory(
            @NonNull final DataSource dataSource
    ) {
        @NotNull final LocalContainerEntityManagerFactoryBean factoryBean =
                new LocalContainerEntityManagerFactoryBean();
        factoryBean.setDataSource(dataSource);
        factoryBean.setJpaVendorAdapter(new HibernateJpaVendorAdapter());
        factoryBean.setPackagesToScan("ru.tsc.tambovtsev.tm");
        @NotNull final Properties properties = new Properties();
        properties.put(Environment.SHOW_SQL, databaseProperties.getShowSql());
        properties.put(Environment.HBM2DDL_AUTO, databaseProperties.getHbm2ddlAuto());
        properties.put(Environment.DIALECT, databaseProperties.getDatabaseSQLDialect());

        properties.put(Environment.FORMAT_SQL, databaseProperties.getFormatSql());
        properties.put(Environment.USE_SECOND_LEVEL_CACHE, databaseProperties.getSecondLvlCash());
        properties.put(Environment.CACHE_REGION_FACTORY, databaseProperties.getFactoryClass());
        properties.put(Environment.USE_QUERY_CACHE, databaseProperties.getUseQueryCash());
        properties.put(Environment.USE_MINIMAL_PUTS, databaseProperties.getUseMinPuts());
        properties.put(Environment.CACHE_REGION_PREFIX, databaseProperties.getRegionPrefix());
        properties.put(Environment.CACHE_PROVIDER_CONFIG, databaseProperties.getConfigFilePath());
        factoryBean.setJpaProperties(properties);
        return factoryBean;
    }

    @Bean
    public PlatformTransactionManager transactionManager(
            @NotNull final LocalContainerEntityManagerFactoryBean entityManagerFactory
    ) {
        @NotNull final JpaTransactionManager transactionManager = new JpaTransactionManager();
        transactionManager.setEntityManagerFactory(entityManagerFactory.getObject());
        return transactionManager;
    }

}
