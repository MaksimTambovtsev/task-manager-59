package ru.tsc.tambovtsev.tm.repository.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Repository;
import ru.tsc.tambovtsev.tm.api.repository.dto.IUserRepository;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;

import java.util.Collection;
import java.util.List;

@Repository
public class UserRepository extends AbstractRepository<UserDTO> implements IUserRepository {

    @NotNull
    protected static final String TABLE_NAME = "UserDTO";

    @NotNull
    @Override
    protected String getTableName() {
        return TABLE_NAME;
    }

    @Override
    public @Nullable UserDTO findById(@Nullable String id) {
        return entityManager.find(UserDTO.class, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByLogin(@Nullable final String login) {
        return getResult(entityManager
                .createQuery("FROM " + TABLE_NAME + " WHERE LOGIN = :login", UserDTO.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("login", login));
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByEmail(@Nullable final String email) {
        return getResult(entityManager
                .createQuery("FROM " + TABLE_NAME + " WHERE EMAIL = :email", UserDTO.class)
                .setHint("org.hibernate.cacheable", true)
                .setParameter("email", email));
    }

    @Override
    @SneakyThrows
    public void lockUserById(@NotNull String id) {
        entityManager
                .createQuery("UPDATE " + TABLE_NAME + " SET LOCKED = 1 WHERE ID = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    @SneakyThrows
    public void unlockUserById(@NotNull String id) {
        entityManager
                .createQuery("UPDATE " + TABLE_NAME + " SET LOCKED = 0 WHERE ID = :id")
                .setParameter("id", id)
                .executeUpdate();
    }

    @Override
    @SneakyThrows
    public void updateUser(@NotNull UserDTO user) {
        entityManager
                .merge(user);
    }

    @Override
    public void addAll(@NotNull Collection<UserDTO> models) {
        entityManager
                .persist(models);
    }

    @Override
    @SneakyThrows
    public void removeByLogin(final String login) {
        entityManager
                .createQuery("DELETE FROM " + TABLE_NAME + " WHERE LOGIN = :login")
                .setParameter("login", login)
                .executeUpdate();
    }

    @Nullable
    @Override
    public List<UserDTO> findAllUser() {
        return entityManager
                .createQuery("FROM " + TABLE_NAME, UserDTO.class)
                .getResultList();
    }

    @Override
    public void clearUser() {
        entityManager
                .createQuery("DELETE FROM " + TABLE_NAME).executeUpdate();
    }

}
