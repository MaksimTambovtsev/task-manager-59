package ru.tsc.tambovtsev.tm.api.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.tambovtsev.tm.dto.model.AbstractEntityDTO;

import javax.persistence.EntityManager;
import java.util.Collection;

public interface IRepository<M extends AbstractEntityDTO> {

    @Nullable
    M findById(@Nullable String id);

    void removeById(@Nullable String id);

    void addAll(@NotNull Collection<M> models);

    void create(@NotNull M model);

    void update(@NotNull M model);

}
