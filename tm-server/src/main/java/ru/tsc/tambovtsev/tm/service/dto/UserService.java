package ru.tsc.tambovtsev.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.tambovtsev.tm.api.repository.dto.IUserRepository;
import ru.tsc.tambovtsev.tm.api.service.dto.IUserService;
import ru.tsc.tambovtsev.tm.api.service.property.ISessionPropertyService;
import ru.tsc.tambovtsev.tm.dto.model.UserDTO;
import ru.tsc.tambovtsev.tm.exception.entity.UserNotFoundException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.util.HashUtil;

import java.util.Collection;
import java.util.List;
import java.util.Optional;

@Service
public class UserService extends AbstractService<UserDTO, IUserRepository> implements IUserService {

    @NotNull
    @Autowired
    private ISessionPropertyService propertyService;

    @Nullable
    @Autowired
    private IUserRepository userRepository;

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @Nullable final UserDTO user = userRepository.findByLogin(login);
        if (user == null) return null;
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    public UserDTO findByEmail(@Nullable final String email) {
        Optional.ofNullable(email).orElseThrow(EmailEmptyException::new);
        @Nullable final UserDTO user = userRepository.findByEmail(email);
        if (user == null) return null;
        return user;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO removeByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        userRepository.removeByLogin(login);
        return null;
    }

    @Override
    public boolean isLoginExists(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return findByLogin(login) != null;
    }

    @Override
    public boolean isEmailExists(@Nullable final String email) {
        if (email == null || email.isEmpty()) return false;
        return findByEmail(email) != null;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public UserDTO setPassword(
            @Nullable final String userId,
            @Nullable final String password
    ) {
        Optional.ofNullable(userId).orElseThrow(UserNotFoundException::new);
        Optional.ofNullable(password).orElseThrow(PasswordEmptyException::new);
        @Nullable final UserDTO user = userRepository.findById(userId);
        if (user == null) return null;
        final String hash = HashUtil.salt(propertyService, password);
        user.setPasswordHash(hash);
        userRepository.updateUser(user);
        return user;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void lockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @Nullable final UserDTO user = findByLogin(login);
        userRepository.lockUserById(user.getId());
    }

    @Override
    @SneakyThrows
    @Transactional
    public void unlockUserByLogin(@Nullable final String login) {
        Optional.ofNullable(login).orElseThrow(LoginEmptyException::new);
        @Nullable final UserDTO user = findByLogin(login);
        userRepository.unlockUserById(user.getId());
    }

    @NotNull
    @Override
    public IUserRepository getRepository() {
        return userRepository;
    }

    @Override
    @Transactional
    public void addAll(@NotNull final Collection<UserDTO> models) {
        userRepository.addAll(models);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<UserDTO> findAll() {
        @Nullable final List<UserDTO> result = userRepository.findAllUser();
        return result;
    }

    @NotNull
    @Override
    @Transactional
    public Collection<UserDTO> set(@NotNull final Collection<UserDTO> models) {
        userRepository.clearUser();
        userRepository.addAll(models);
        return models;
    }

}
