package ru.tsc.tambovtsev.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.tambovtsev.tm.api.repository.model.IRepository;
import ru.tsc.tambovtsev.tm.api.service.model.IService;
import ru.tsc.tambovtsev.tm.exception.field.IdEmptyException;
import ru.tsc.tambovtsev.tm.model.AbstractEntity;

import java.util.Collection;
import java.util.Optional;

@Service
public abstract class AbstractGraphService<M extends AbstractEntity, R extends IRepository<M>> implements IService<M> {

    @NotNull
    public abstract IRepository<M> getRepository();

    @Override
    public abstract void addAll(@NotNull final Collection<M> models);

    @Nullable
    @Override
    @SneakyThrows
    public M findById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IRepository<M> repository = getRepository();
        @Nullable final M result = repository.findById(id);
        return result;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String id) {
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IRepository<M> repository = getRepository();
        @Nullable final M result = repository.findById(id);
        if (result == null) return;
        repository.removeById(id);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void create(@NotNull final M model) {
        Optional.ofNullable(model).orElseThrow(NullPointerException::new);
        @NotNull final IRepository<M> repository = getRepository();
        repository.create(model);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void update(@NotNull final M model) {
        @NotNull final IRepository<M> repository = getRepository();
        repository.update(model);
    }

    @Override
    @Transactional
    public void removeCascade(@NotNull M model) {
        @NotNull final IRepository<M> repository = getRepository();
        repository.removeCascade(model);
    }

}
