package ru.tsc.tambovtsev.tm.api.service.property;

import org.jetbrains.annotations.NotNull;
import ru.tsc.tambovtsev.tm.api.component.ISaltProvider;

public interface ISessionPropertyService extends ISaltProvider {

    @NotNull
    String getSessionKey();

    @NotNull
    Integer getSessionTimeout();

}
