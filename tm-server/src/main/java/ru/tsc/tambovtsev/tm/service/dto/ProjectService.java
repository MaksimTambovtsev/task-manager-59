package ru.tsc.tambovtsev.tm.service.dto;


import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.tambovtsev.tm.api.repository.dto.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.service.dto.IProjectService;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.dto.model.ProjectDTO;

import java.util.*;

@Service
public final class ProjectService extends AbstractUserOwnedService<ProjectDTO, IProjectRepository> implements IProjectService {

    @Autowired
    private IProjectRepository repository;

    @NotNull
    @Override
    public IProjectRepository getRepository() {
        return repository;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDTO changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        final ProjectDTO project = repository.findById(userId, id);
        if (project == null) return null;
        project.setStatus(status);
        project.setUserId(userId);
        repository.updateById(project);
        return project;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
            @Nullable final ProjectDTO result = repository.findById(userId, id);
            if (result == null) return;
            repository.removeByIdProject(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll(
            @Nullable final String userIdPr,
            @Nullable final Sort sort
    ) {
        Optional.ofNullable(userIdPr).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(sort).orElseThrow(NameEmptyException::new);
        return repository.findAll(userIdPr, sort);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<ProjectDTO> findAll() {
        return repository.findAllProject();
    }

    @Override
    @Transactional
    public void addAll(@NotNull final Collection<ProjectDTO> models) {
        repository.addAll(models);
    }

    @Override
    @Transactional
    public void clearProject() {
        repository.clearProject();
    }

    @NotNull
    @Override
    @Transactional
    public Collection<ProjectDTO> set(@NotNull final Collection<ProjectDTO> models) {
        repository.clearProject();
        repository.addAll(models);
        return models;
    }

}
