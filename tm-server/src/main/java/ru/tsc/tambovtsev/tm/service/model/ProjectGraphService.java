package ru.tsc.tambovtsev.tm.service.model;


import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.tambovtsev.tm.api.repository.model.IProjectRepository;
import ru.tsc.tambovtsev.tm.api.service.model.IProjectService;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.model.Project;

import java.util.*;

@Service
public class ProjectGraphService extends AbstractUserOwnedGraphService<Project, IProjectRepository> implements IProjectService {

    @Nullable
    @Autowired
    private IProjectRepository repository;

    @NotNull
    @Override
    public IProjectRepository getRepository() {
        return repository;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Project changeProjectStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        final Project project = repository.findById(userId, id);
        if (project == null) return null;
        project.setStatus(status);
        repository.updateById(project);
        return project;
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(
            @Nullable final String userId,
            @Nullable final String id
    ) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @Nullable final Project result = repository.findById(userId, id);
        if (result == null) return;
        repository.removeByIdProject(userId, id);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Project> findAll(
            @Nullable final String userIdPr,
            @Nullable final Sort sort
    ) {
        Optional.ofNullable(userIdPr).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(sort).orElseThrow(NameEmptyException::new);
        return repository.findAll(userIdPr, sort);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Project> findAll() {
        return repository.findAllProject();
    }

    @Override
    @Transactional
    public void addAll(@NotNull final Collection<Project> models) {
        repository.addAll(models);
    }

    @Override
    @Transactional
    public void clearProject() {
        repository.clearProject();
    }

    @NotNull
    @Override
    @Transactional
    public Collection<Project> set(@NotNull final Collection<Project> models) {
        repository.clearProject();
        repository.addAll(models);
        return models;
    }

}
