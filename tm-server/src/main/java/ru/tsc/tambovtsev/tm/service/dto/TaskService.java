package ru.tsc.tambovtsev.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.tambovtsev.tm.api.repository.dto.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.service.dto.ITaskService;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.dto.model.TaskDTO;

import java.util.*;

@Service
public class TaskService extends AbstractUserOwnedService<TaskDTO, ITaskRepository> implements ITaskService {

    @Autowired
    private ITaskRepository repository;

    @NotNull
    @Override
    public ITaskRepository getRepository() {
        return repository;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        @Nullable final TaskDTO task = repository.findById(userId, id);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        task.setUserId(userId);
        repository.updateById(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public TaskDTO changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        final TaskDTO task = repository.findById(userId, id);
        if (task == null) return null;
        task.setStatus(status);
        task.setUserId(userId);
        repository.updateById(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll(
            @Nullable final String userIdPr,
            @Nullable final Sort sort
    ) {
        Optional.ofNullable(userIdPr).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(sort).orElseThrow(NameEmptyException::new);
        return repository.findAll(userIdPr, sort);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<TaskDTO> findAll() {
        return repository.findAllTask();
    }

    @Override
    @Transactional
    public void addAll(@NotNull final Collection<TaskDTO> models) {
        repository.addAll(models);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<TaskDTO> set(@NotNull final Collection<TaskDTO> models) {
        repository.clearTask();
        repository.addAll(models);
        return models;
    }

    @Override
    @Transactional
    public void clearTask() {
        repository.clearTask();
    }

}
