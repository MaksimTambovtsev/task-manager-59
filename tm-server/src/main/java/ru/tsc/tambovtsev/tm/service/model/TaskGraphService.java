package ru.tsc.tambovtsev.tm.service.model;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.tambovtsev.tm.api.repository.model.ITaskRepository;
import ru.tsc.tambovtsev.tm.api.service.model.ITaskService;
import ru.tsc.tambovtsev.tm.enumerated.Sort;
import ru.tsc.tambovtsev.tm.enumerated.Status;
import ru.tsc.tambovtsev.tm.exception.AbstractException;
import ru.tsc.tambovtsev.tm.exception.field.*;
import ru.tsc.tambovtsev.tm.model.Task;

import java.util.*;

@Service
public class TaskGraphService extends AbstractUserOwnedGraphService<Task, ITaskRepository> implements ITaskService {

    @Nullable
    @Autowired
    private ITaskRepository repository;

    @NotNull
    @Override
    public ITaskRepository getRepository() {
        return repository;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task updateById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final String name,
            @Nullable final String description
    ) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        Optional.ofNullable(name).orElseThrow(NameEmptyException::new);
        @Nullable final Task task = repository.findById(userId, id);
        if (task == null) return null;
        task.setName(name);
        task.setDescription(description);
        repository.updateById(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    @Transactional
    public Task changeTaskStatusById(
            @Nullable final String userId,
            @Nullable final String id,
            @Nullable final Status status
    ) throws AbstractException {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        final Task task = repository.findById(userId, id);
        if (task == null) return null;
        task.setStatus(status);
        repository.updateById(task);
        return task;
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAll(
            @Nullable final String userIdPr,
            @Nullable final Sort sort
    ) {
        Optional.ofNullable(userIdPr).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(sort).orElseThrow(NameEmptyException::new);
        return repository.findAll(userIdPr, sort);
    }

    @Nullable
    @Override
    @SneakyThrows
    public List<Task> findAll() {
        return repository.findAllTask();
    }

    @Override
    @Transactional
    public void addAll(@NotNull final Collection<Task> models) {
        repository.addAll(models);
    }

    @NotNull
    @Override
    @Transactional
    public Collection<Task> set(@NotNull final Collection<Task> models) {
        repository.clearTask();
        repository.addAll(models);
        return models;
    }

}
