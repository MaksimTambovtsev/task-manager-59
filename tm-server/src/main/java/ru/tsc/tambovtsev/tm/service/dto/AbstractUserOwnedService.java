package ru.tsc.tambovtsev.tm.service.dto;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.tambovtsev.tm.api.repository.dto.IOwnerRepository;
import ru.tsc.tambovtsev.tm.api.service.dto.IUserOwnedService;
import ru.tsc.tambovtsev.tm.exception.field.IdEmptyException;
import ru.tsc.tambovtsev.tm.exception.field.UserIdEmptyException;
import ru.tsc.tambovtsev.tm.dto.model.AbstractUserOwnedModelDTO;

import java.util.Optional;

@Service
public abstract class AbstractUserOwnedService<M extends AbstractUserOwnedModelDTO, R extends IOwnerRepository<M>>
        extends AbstractService<M, R> implements IUserOwnedService<M> {

    @NotNull
    public abstract IOwnerRepository<M> getRepository();

    @Override
    @SneakyThrows
    @Transactional
    public void clear(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        @NotNull final IOwnerRepository<M> repository = getRepository();
        repository.clear(userId);
    }

    @Nullable
    @Override
    @SneakyThrows
    public M findById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IOwnerRepository<M> repository = getRepository();
        @Nullable final M result = repository.findById(userId, id);
        return result;
    }

    @Override
    @SneakyThrows
    public long getSize(@Nullable final String userId) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        @NotNull final IOwnerRepository<M> repository = getRepository();
        return repository.getSize(userId);
    }

    @Override
    @SneakyThrows
    @Transactional
    public void removeById(@Nullable final String userId, @Nullable final String id) {
        Optional.ofNullable(userId).orElseThrow(UserIdEmptyException::new);
        Optional.ofNullable(id).orElseThrow(IdEmptyException::new);
        @NotNull final IOwnerRepository<M> repository = getRepository();
        @Nullable final M result = repository.findById(userId, id);
        if (result == null) return;
        repository.removeById(userId, id);
    }

}
